#### バックアップパスがあればできること
- ファストコンバージェンス

---

#### バックアップパスを伝える手段
- BGP add-path
 - BGPプロトコル拡張

- BGP Best External + Fullmesh
 - ASBRの機能拡張

- BGP Diverse path
 - RR(ルートリフレクタ)の機能拡張

---

### BGP add-path
- Junos OS 11.3以降
- H/W制限なし  
add-path:  
https://www.juniper.net/documentation/en_US/junos/topics/reference/configuration-statement/add-path-edit-protocols-bgp.html

---

```
add-path {  
    receive;  
    send {  
        multipath;  
        path-count number;  
        prefix-policy [ policy-names ];  
    }  
}  
```

---

### BGP Advertise Best External
- Junos OS 9.3R1以降  
- H/W制限なし  
advertise-external:  
https://www.juniper.net/techpubs/en_US/junos/topics/reference/configuration-statement/advertise-external-edit-protocols-bgp.html

---
#### 1. BGP Advertise Best Externalの設定

```
[edit protocols bgp group group-name neighbor address],
user@host# set advertise-external {conditional};
```

---

### BGP Prefix Independent Convergenceとは
- RIBからベストパス、バックアップパスとなる経路を選出
- FIBにはベストパスとなるネクストホップに加え、バックアップパスのネクストホップをインストール
- ベストパスのネクストホップが無効になったら、バックアップパスでの転送に切り替える

---

### BGP Prefix Independent Convergence
- Junos OS ***15.1R1***以降
- MPCのみ利用可能(DPC等は使用不可)  
Configuring BGP Prefix Independent Convergence for Inet:  
https://www.juniper.net/techpubs/en_US/junos/topics/task/configuration/bgp-configuring-bgp-pic-for-inet.html

---

#### 1. test
```
[edit chassis network-services]  
user@host# set enhanced-ip
```

+++

#### 2. Enable BGP PIC for inet.  
```
[edit routing-options]  
user@host# set protect core  
```

+++

#### 3. Configure per-packet load balancing.

```
[edit policy-options]  
user@host# set policy-statement policy-name then load-balance per-packet  
```

+++

#### 4. Apply the per-packet load-balancing policy to routes exported from the routing table to the forwarding table.  

```
[edit routing-options forwarding-table]  
user@host# set export policy-name
```

+++

#### 5. Verify that BGP PIC is working.  
From operational mode, enter the show route extensive command:

The output lines that contain Indirect next hop:  
weight follow next hops that the software can use to repair paths where a link failure occurs.  
The next-hop weight has one of the following values:  

```  
0x1 indicates active next hops.  
0x4000 indicates passive next hops.
```

---

#### ネクストホップの解決にスタティック,BGPを使わない
```
[edit policy-options]
policy-statement next-hop-resolution {
    term static {
        from protocol static;
        then reject;
    }
    term bgp {
        from protocol bgp;
        then reject;
    }
    term any {        ###確認中
        then accept;  ###確認中
    }
}

[edit routing-options]
resolution {
    rib inet.0 {
        import next-hop-resolution;
    }
}

```

---

#### 検証方法

1. コンバージェンスの時間測定
2. MX240のバージョンアップ
3. BGP PIC導入
4. Best External Path開始
5. 障害試験

---